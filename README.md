# xlatexeditor


## Screenshot

![](xlatexeditor.png)


## Fork

Little fork of mupdf to use vim and x11 to view a pdf while you type. 



## Keys
v : edit with vim editor (using tinynohup).

c : recompile using pdflatex and refresh the PDF document.

F12: Quit the soft.


## Features

The xlatexeditor allows to use the basic commands of vim. 
The advantage is for instance to use multiple monitors. One monitor can be set to type and the other second monitor will display the PDF in-situ, while typing. 
It needs SSH to be enabled. 



