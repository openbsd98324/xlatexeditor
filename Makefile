

all: 
	  echo COMPILE  
	  cc src/tinynohup.c  -o  /tmp/tinynohup 
	  cd src  ; make 
	  ls   src/build/release/mupdf* 



install:
	  echo installation procedure 
	  echo INSTALL 
	  cp  src/build/release/mupdf-x11   /usr/local/bin/xlatexeditor   


ls:
	  ls  -ltra  src/build/release/mupdf-x11   
	  ls  -ltra  /usr/local/bin/xlatexeditor   
	  du  -hs  src/build/release/mupdf-x11   
	  du  -hs  /usr/local/bin/xlatexeditor   


tinynohup:
	  cc src/tinynohup.c  -o  /tmp/tinynohup 


clean: 
	  echo COMPILE  
	  cd src  ; make clean 
	  rm  src/build/release/mupdf-x11 

run: 
	  echo COMPILE  
	  cc src/tinynohup.c  -o /tmp/tinynohup 
	  cd src  ; make 
	  pdflatex test.tex 
	  src/build/release/mupdf-x11   test.pdf 




lib:
	  apt-get update  ; apt-get install -y libx11-dev  git  libxi-dev libxrandr-dev  gcc clang g++ make 



ed:
	  echo edit 
	   echo vim src/platform/x11/x11_main.c
	   vim src/platform/x11/x11_main.c
	   echo vim src/platform/x11/x11_main.c


less:
	   less src/platform/x11/x11_main.c

totmp:
	   cp src/platform/x11/x11_main.c  /tmp



edit:
	  vim src/platform/x11/x11_main.c

edits:
	  vim src/platform/x11/*.c -p 

edith:
	  vim src/platform/x11/*.h -p 

editmain:
	  echo vim src/platform/gl/gl-main.c
	  vim src/platform/x11/*app*.c


cln: 
	  rm src/build/release/platform/x11/*.o



